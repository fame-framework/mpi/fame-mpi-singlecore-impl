package de.dlr.gitlab.fame.mpi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.logging.Logging;

/** Implements {@link MpiFacade} without any parallelisation
 * 
 * @author Christoph Schimeczek, Achraf El Ghazi */
public class SingleCoreImpl implements MpiFacade {
	static final String MUST_NOT_CALL = "This method is not implemented in Single-Core-Mode and must not be called!";

	private static final Logger LOGGER = LoggerFactory.getLogger(SingleCoreImpl.class);

	@Override
	public MpiMode getMode() {
		return MpiMode.SINGLE_CORE;
	}

	@Override
	public int getRank() {
		return 0;
	}

	@Override
	public int getSize() {
		return 1;
	}

	@Override
	public String[] initialise(String[] args) {
		return args;
	}

	@Override
	public void invokeFinalize() {}

	@Override
	public byte[] broadcastBytes(byte[] data, int source) {
		return data;
	}

	@Override
	public void sendBytesTo(byte[] data, int target, int tag) {
		throw Logging.logFatalException(LOGGER, MUST_NOT_CALL);
	}

	@Override
	public byte[] receiveBytesWithTag(int tag) {
		throw Logging.logFatalException(LOGGER, MUST_NOT_CALL);
	}

	@Override
	public MpiRequestFacade iSendBytesTo(byte[] data, int target, int tag) {
		throw Logging.logFatalException(LOGGER, MUST_NOT_CALL);
	}
}
